//
//  ViewController.m
//  DuetCalc
//
//  Created by Andrii Tymchenko on 2/20/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (assign, nonatomic) NSInteger  summma;

@property (strong, nonatomic)   NSMutableArray *operations;

@property (strong, nonatomic) NSString  *string;




@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (weak, nonatomic) IBOutlet UITextField *inputTextField;

@property (weak, nonatomic) IBOutlet UIButton *addButtonPlus;
@property (weak, nonatomic) IBOutlet UIButton *addButtonMinus;

@property (weak, nonatomic) IBOutlet UIButton *addButtonMultiply;
@property (weak, nonatomic) IBOutlet UIButton *addButtonDivide;

//@[@"0+5=5", @"5*7=35", @"35-10=25"]

@end



@implementation ViewController



- (IBAction)addButtonDidPushed:(id)sender {
    NSLog(@"addButtonDidPushedPlus");
    
    //отримуємо текст з ui
    NSString *input = self.inputTextField.text;
    
    //отримуємо числове значення з тексту, якщо там буде не число
    // скоріше всього програма упаде
    NSInteger intValue = [input integerValue];
    
    
    self.summma = self.summma + intValue;
    
    
    NSString *operation = [[NSString alloc] initWithFormat:@"%li + %li = %li", (self.summma - intValue), (long)intValue, (long)self.summma];
    NSLog(@"operation %@", operation);
    [self.operations addObject: operation];
    
    
    self.resultLabel.text = [NSString stringWithFormat:@"Result: %ld",self.summma];
    
}
- (IBAction)addButtonDidPushedMinus:(id)sender {
    NSLog(@"addButtonDidPushedMinus");
    
    NSString *input = self.inputTextField.text;
    NSInteger intValue  = [input integerValue ];
    self.summma = self.summma - intValue;
    
    NSString *operation = [[NSString alloc] initWithFormat:@"%li - %li = %li", (self.summma + intValue), (long)intValue, (long)self.summma];
    
    NSLog(@"operation %@",operation);
    
    [self.operations addObject:operation];
    
    
    //NSString *operation = @"%li - %li = %li", (self.summma - intValue), (long)intValue,(long) self.summma;
    //?????????????????????????????????
    //?????????????????????
    //??????????
    
    self.resultLabel.text = [NSString stringWithFormat:@"Result: %ld",self.summma];
}
- (IBAction)addButtonDidPushedMultiply:(id)sender {
    NSLog(@"addButtonDidPushedMultiply");
    
    NSString *input = self.inputTextField.text;
    NSInteger intValue = [input integerValue];
    self.summma = self.summma * intValue;
    NSString *operation = [[NSString alloc] initWithFormat:@"%li * %li = %li", (self.summma / intValue), (long)intValue, (long)self.summma];
    
    NSLog(@"operation %@",operation);
    
    [self.operations addObject:operation];
    
    
    self.resultLabel.text = [NSString stringWithFormat:@"Result: %ld",self.summma];
}
- (IBAction)addButtonDidPushedDivide:(id)sender {
    
    NSString *input = self.inputTextField.text;
    NSInteger intValue = [input integerValue];
    self.summma = self.summma / intValue;
    
    NSLog(@"addButtonDidPushedDivide");
    
    
    NSString *operation = [[NSString alloc] initWithFormat:@"%li / %li = %li", (self.summma * intValue), (long)intValue, (long)self.summma];
    
    NSLog(@"operation %@",operation);
    
    [self.operations addObject:operation];
    
    self.resultLabel.text = [NSString stringWithFormat:@"Result: %ld",self.summma];
}






- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.operations = [[NSMutableArray alloc] init];
    
    [self.operations addObject: @"Count"];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    self.summma = 0;
    
    NSLog(@"additional line of code for commit");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
